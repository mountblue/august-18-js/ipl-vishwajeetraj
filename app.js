const csvToJson = require('csvtojson');
const fs = require('fs');
const ipl = require('./ipl');
const matchesCsv = './data/matches.csv';
const deliveriesCsv = './data/deliveries.csv';


Promise.all([
    csvToJson().fromFile(matchesCsv),
    csvToJson().fromFile(deliveriesCsv),
]).then((allData) => {
    const matches = allData[0];
    const deliveries = allData[1];
    // ************Code For Problem 1*******************
    const counts = ipl.countPerYearMatch(matches);

    // ************Code For Problem 2*******************
    const yearData = ipl.getAllYears(matches);
    const teamEachYearData = ipl.teamsData(yearData, matches);

    // ************Code For Problem 3*******************
    const matchId = ipl.getMatchId(matches, '2016');
    const extraRun = ipl.generateExtraRunObject(matchId, deliveries);

    // ************Code For Problem 4 *******************
    const matchId2015 = ipl.getMatchId(matches, '2015');
    const allBowlerData = ipl.getAllBowlersBallAndRun(matchId2015, deliveries);
    // console.log(allBowlerData);
    const topTenEconomyBowler = ipl.getTopEconomyBowler(allBowlerData);

    const appJson = {};
    // ---for adding match played per year object to appJson object---
    appJson.matchesPerYear = counts;
    // ---Adding the extra run object to appJson Object--------
    appJson.extraRunPerTeam = extraRun;
    // ---for Adding perYearPerTeam Object to appJson object-----
    appJson.perYearPerTeamData = teamEachYearData;
    // ---Adding top Economy bowler's Data object to appJson object-------
    appJson.topTenBowler = topTenEconomyBowler;

    fs.writeFile(
        './public/app.json',
        JSON.stringify(appJson),
        (err) => {
            if (err) console.log(err);
            // console.log('Successfully Written to File.');
        },
    );
});