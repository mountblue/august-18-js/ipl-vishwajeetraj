const fs = require('fs');
module.exports = {
    // ------Function for Extracting Top Economy Bowler-------
    getTopEconomyBowler: function(input) {
        const resultObj = {};

        for (const element in input) {
            resultObj[element] = parseFloat(input[element][0] / (input[element][1] / 6)).toFixed(2);
        }
        // console.log(resultObj)
        const sortable = Object.entries(resultObj).sort((a, b) => a[1] - b[1]).slice(0, 10);
        let finalData = Object.assign(...sortable.map(d => ({
            [d[0]]: parseFloat(d[1]),
        })));

        return finalData;
    },

    // ------Function for Extracting all Bowler's Data of Year 2015--------
    getAllBowlersBallAndRun: function(arr, deliveries) {
        return deliveries.reduce((resultObj, element) => {
            if (arr.includes(element.match_id)) {
                if (resultObj.hasOwnProperty(element.bowler)) {
                    resultObj[element.bowler][0] += parseInt(element.total_runs);
                    resultObj[element.bowler][1] += 1;
                } else {
                    resultObj[element.bowler] = [parseInt(element.total_runs), parseInt(element.ball)];
                }
            }
            return resultObj;
        }, {});
    },

    // ------For generating extra Run given by each team in 2016-------
    generateExtraRunObject: function(arr, deliveries) {
        return deliveries.reduce((resultObj, element) => {
            if (arr.includes(element.match_id)) {
                if (resultObj.hasOwnProperty(element.bowling_team)) {
                    resultObj[element.bowling_team] += parseInt(element.extra_runs);
                } else {
                    resultObj[element.bowling_team] = parseInt(element.extra_runs);
                }
            }
            return resultObj;
        }, {});
    },

    // ---For getting all match Id of 2016-----
    getMatchId: function(inputObj, season) {
        const filteredId = [];
        inputObj.filter((element) => {
            if (element.season === season) {
                filteredId.push(element.id);
            }
        });
        return filteredId;
    },

    // ---for Extracting The match won by each team per year-----
    teamsData: function(yearArray, matchData) {
        const matchesWonPerYear = matchData.reduce((newObject, element) => {
            if (element.winner !== '') {
                if (newObject.hasOwnProperty(element.winner)) {
                    newObject[element.winner][element.season] += 1;
                } else {
                    const yearMatches = {};
                    yearArray.map((year) => {
                        yearMatches[year] = 0;
                    });
                    yearMatches[element.season] = 1;
                    newObject[element.winner] = yearMatches;
                }
            }
            return newObject;
        }, {});

        return matchesWonPerYear;
    },

    // ----Function for getting all Year in sorted format-----
    getAllYears: function(input) {
        const uniqueYear = [...new Set(input.map(item => item.season))];
        // --For sorting Data--//
        const sortedData = uniqueYear.sort((a, b) => {
            if (a > b) {
                return 1;
            }
            return -1;
        });
        return sortedData;
    },



    // ----Function for counting Match per year-----//
    countPerYearMatch: function(input) {
        // console.log(input);
        return input.reduce((counts, element) => {
            if (counts.hasOwnProperty(element.season)) {
                counts[element.season] += 1;
            } else {
                counts[element.season] = 1;
                //teamsData
            }
            return counts;
        }, {});
    }



};