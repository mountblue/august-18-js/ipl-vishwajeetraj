module.exports = {

    "extends": ["airbnb"],
    "rules": {
        "no-undef": 0,
        "prefer-destructuring": 0,

    },
    "plugins": ["prettier"]
}