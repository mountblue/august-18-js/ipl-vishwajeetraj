$.getJSON('app.json', (element) => {
  const matchPlayedPerYear = {
    chart: {
      type: 'column',
    },
    title: {
      text: 'IPL data( Match Played Per Year)',
    },
    xAxis: {
      categories: [],
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Total match per Year',
      },
      stackLabels: {
        enabled: true,
        style: {
          fontWeight: 'bold',
        },
      },
    },
    legend: {
      align: 'right',
      x: -30,
      verticalAlign: 'top',
      y: 25,
      floating: true,
      backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
      borderColor: '#CCC',
      borderWidth: 1,
      shadow: false,
    },
    tooltip: {
      headerFormat: '<b>{point.x}</b><br/>',
      // pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
    },
    plotOptions: {
      column: {
        stacking: 'normal',
        dataLabels: {
          enabled: true,
          color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
        },
      },
    },
    series: [{
      name: 'Matches',
      data: [],
    }],
  };

  const newLocal = element.matchesPerYear;
  const matchesPerYear = newLocal;
  matchPlayedPerYear.xAxis.categories = Object.keys(matchesPerYear);
  matchPlayedPerYear.series[0].data = Object.values(matchesPerYear);
  Highcharts.chart('matchPerYearContainer', matchPlayedPerYear);

  // For Per Year Per Team
  const matchWinnerPerYear = {

    chart: {
      type: 'column',
    },

    title: {
      text: 'IPL Data (Maches Won By Each Team) ',
    },

    xAxis: {
      categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas'],
    },

    yAxis: {
      allowDecimals: false,
      min: 0,
      title: {
        text: 'Match Won Per Year Per Team',
      },
    },

    tooltip: {
      formatter() {
        return `<b>${this.x}</b><br/>${
          this.series.name}: ${this.y}<br/>`
                    + `Total: ${this.point.stackTotal}`;
      },
    },

    plotOptions: {
      column: {
        stacking: 'normal',
      },
    },

    series: [],
  };


  const perYearPerTeamData = element.perYearPerTeamData;
  matchWinnerPerYear.xAxis.categories = Object.keys(matchesPerYear);
  const key = Object.keys(perYearPerTeamData);
  key.forEach((input) => {
    const info = {};
    info.name = input;
    info.data = (Object.values(perYearPerTeamData[input]));

    matchWinnerPerYear.series.push(info);
  });
  Highcharts.chart('perYearMatchWinner', matchWinnerPerYear);


  // For Top Ten Bolers;
  const bowler = {
    chart: {
      type: 'column',
    },
    title: {
      text: 'IPL data (Top Economy Bowler of 2015)',
    },
    subtitle: {
      text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>',
    },
    xAxis: {
      type: 'category',
    },
    yAxis: {
      title: {
        text: 'Top Economy Bowler- 2015',
      },

    },
    legend: {
      enabled: false,
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        dataLabels: {
          enabled: true,
          format: '{point.y:.1f}%',
        },
      },
    },

    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>',
    },

    series: [{
      name: 'Economy',
      data: [],
    }],
  };


  const topTenBowler = element.topTenBowler;
  bowler.xAxis.categories = Object.keys(topTenBowler);
  //   console.log(Object.values(data));
  bowler.series[0].data = Object.values(topTenBowler);
  Highcharts.chart('economyBowler', bowler);


  // // For Extra Run given by Each team in year 2016
  const extraRun = {
    chart: {
      type: 'column',
    },
    title: {
      text: 'IPL data( Extra Run Given By Each Team in YEAR 2016 )',
    },
    xAxis: {
      categories: [],
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Extra Run Per Team for 2016',
      },
      stackLabels: {
        enabled: true,
        style: {
          fontWeight: 'bold',
          color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray',
        },
      },
    },
    legend: {
      align: 'right',
      x: -30,
      verticalAlign: 'top',
      y: 25,
      floating: true,
      backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
      borderColor: '#CCC',
      borderWidth: 1,
      shadow: false,
    },
    tooltip: {
      headerFormat: '<b>{point.x}</b><br/>',

    },
    plotOptions: {
      column: {
        stacking: 'normal',
        dataLabels: {
          enabled: true,
          // color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
        },
      },
    },
    series: [{
      name: 'Extra Runs',
      data: [],
    }],
  };

  const extraRunPerTeam = element.extraRunPerTeam;
  extraRun.xAxis.categories = Object.keys(extraRunPerTeam);
  extraRun.series[0].data = Object.values(extraRunPerTeam);
  Highcharts.chart('extraRunByTeams', extraRun);
});
