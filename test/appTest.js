const expect = require('chai').expect;
const ipl = require('../ipl');
describe('IPL-Data', () => {


    describe('Match Per Year', () => {
        it('function Existence', () => {
            expect(ipl.countPerYearMatch).to.be.a('function');
        });


        const obj = [{ season: 2010 }, { season: 2011 }, { season: 2011 }];
        const result = { 2010: 1, 2011: 2 };

        it('function must count the occurance', () => {
            expect(ipl.countPerYearMatch(obj)).to.deep.equal(result);
        });
    });
    const ip = [{

        season: 2016,
    }, {

        season: 2016,

    }, {

        season: 2011,

    }, {

        season: 2016,

    }, {

        season: 2011,

    }, {
        season: 2010,

    }];
    const res = [2010, 2011, 2016];
    describe('Match won by Each Team Per Year', () => {
        it('should contain function for extracting all unique Year', () => {
            expect(ipl.getAllYears).to.be.a('function');
        });
        it('Function should return only unique data', () => {
            expect(ipl.getAllYears(ip)).to.deep.equal(res);
        });
    });

    const expResult = [1, 2, 5, 7];
    const input = [{
            id: 1,
            season: '2016',
        }, {
            id: 2,
            season: '2016',
        }, {
            id: 3,
            season: '2017',

        }, {
            id: 4,
            season: '2015',

        }, {
            id: 5,
            season: '2016',

        },

        {
            id: 6,
            season: '2015',

        },

        {
            id: 7,
            season: '2016',

        },
    ];

    describe('Extra Run By Each Team For Year 2016', () => {
        it('should contain function for filtering Match Id', () => {
            expect(ipl.getMatchId).to.be.a('function');
        });
        it('Function should return match id For year 2016', () => {
            expect(ipl.getMatchId(input, '2016')).to.deep.equal(expResult);
        });
    });
    // getMatchId
    const expResults = [4, 6];
    describe("Top Economy Bowler's", () => {
        it('should contain function for extracting Bowler data', () => {
            expect(ipl.getAllBowlersBallAndRun).to.be.a('function');
        });
        it('should return match id For year 2015', () => {
            expect(ipl.getMatchId(input, '2015')).to.deep.equal(expResults);
        });
    });
});