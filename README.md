# IPL Data Visualization

This project will visualize IPL data using Graphs



## Getting Started
-Download or Clone this Project to your Local machine.


### Prerequisites
Install Node.js.<br />
If that's the first time you work with Node.js<br />
Ubuntu Linux<br />
Open terminal.<br />
Install Node.js runtime. >sudo apt-get install node.<br />
Install npm (node package manager) > sudo apt-get install npm.<br />


### Installing

Move your terminal to project folder where app.js pages live: cd <path-to-content> <br />
node app.js



## Deployment
Install live-server: npm install -g live-server.<br />
Move your terminal to public folder where index.html pages live: cd <path-to-content><br />
Start the server: live-server .<br />
Open localhost:8080 in a browser.<br />


```
sample:
```
![Screenshot_from_2018-09-07_15-00-49](/uploads/6f7eb04c7e62359a63381d218c4d844a/Screenshot_from_2018-09-07_15-00-49.png)
<br />
![Screenshot_from_2018-09-07_15-00-54](/uploads/abd2e44a8f0cba97d048845e301f5cf8/Screenshot_from_2018-09-07_15-00-54.png)

## Built With

* [Html/Css](http://www.devdocs.io/) - Used for front End
* [Mocha & chai](https://mochajs.org) - For testing
* [Highcharts](https://www.highcharts.com/stock/demo) -Highcharts library for Rendering Graph
* [javascript and Jquerry](http://www.devdocs.io/) - Used for backend